public class SLLNode {
    Integer key;
    
    SLLNode next;
    
    public SLLNode(Integer key, SLLNode next)
    {
        this.key = key;
        this.next = next;
    }
    
    SLLNode(int key)
    {
        this(key, null);
    }
    
    SLLNode()
    {
        this(null, null);
    }
}
