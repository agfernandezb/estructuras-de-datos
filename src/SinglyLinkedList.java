import java.util.EmptyStackException;

public class SinglyLinkedList {
    protected SLLNode head;
    protected SLLNode tail;
    protected int size;
    
    public SinglyLinkedList()
    {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }
    
    public boolean empty()
    {
        return(size==0);
    }
    
    public int size()
    {
        return(size);
    }
    
    public void pushFront(int key)
    {
        SLLNode node = new SLLNode(key,head);
        this.head = node;
        if(tail==null){
            this.tail = head;
        }
        this.size++;
    }
    
    public SLLNode popFront()
    {
        if(size==0){
            throw new EmptyStackException();
        }
        SLLNode oldHead = this.head;
        this.head = head.next;
        if(head==null){
            this.tail = null;
        }
        this.size--;
        return oldHead;
    }
    
    public void pushBack(int key)
    {
        SLLNode node = new SLLNode(key,null);
        this.tail = node;
        if(tail==null){
            this.head = node;
            this.tail = node;
        } else{
            this.tail.next = node;
            this.tail = node;
        }
        this.size++;
    }
    
    public SLLNode popBack()
    {
        if(size==0){
            throw new EmptyStackException();
        }
        SLLNode oldTail = this.tail;
        if(head==tail){
            this.head = null;
            this.tail = null;
        } else{
            SLLNode node = head;
            while(node.next.next!=null){
                node = node.next;
            }
            node.next = null;
            this.tail = node;
        }
        this.size--;
        return oldTail;
    }
    
    public void addAfter(int key, SLLNode node1){
        SLLNode node2 = new SLLNode(key,node1.next);
        node1.next = node2;
        if(tail==node1){
            this.tail = node2;
        }
        this.size++;
    }
    public void addBefore(int key, SLLNode node2){
        SLLNode node1 = new SLLNode(key,node2);
        if(head==node2){
            this.head = node1;
        } else{
            SLLNode node = head;
            while(node.next!=node2){
                node = node.next;
            }
            node.next = node1;
        }
        this.size++;
    }
    public int peekFront(){
        if(size==0){
            throw new EmptyStackException();
        }
        return(this.head.key);
    }
    public int peekBack(){
        if(size==0){
            throw new EmptyStackException();
        }
        return(this.tail.key);
    }
}